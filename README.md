A collection of assorted shell scripts I've written that other people might find
useful.

- `chusername`: Automate changing username, home directory, and full name
on Linux systems. Should also work on FreeBSD, but has not been tested outside
of Linux. Run on other unices at your own risk.
    - Dependencies: none
- `gdget`: Download files from Google Drive using `wget`. Supports downloading
over Tor using `torsocks`.
    - Dependencies: wget, torsocks (optional)
- `serve`: Put up a temporary minimal web server to transfer a file from one 
device to another.
    - Dependencies: netcat
- `whonixinstall`: Automatically download and install the latest version of 
[Whonix](https://www.whonix.org/) libvirt. Supports downloading over Tor.
    - Dependencies: wget, torsocks (optional), libvirt, qemu, gpg